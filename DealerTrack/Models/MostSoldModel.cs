﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealerTrack.Models
{
    public class MostSoldModel
    {
        public string Vehicle { get; set; }
        public int NumberOfVehiclesSold { get; set; }

    }
}
