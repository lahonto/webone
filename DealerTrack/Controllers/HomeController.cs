﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DealerTrack.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using DealerTrack.Core;

namespace DealerTrack.Controllers
{
    public class HomeController : Controller
    {
        //List<VehicleSalesModel> salesDetails = new List<VehicleSalesModel>();
        //private Core.VehicleSales salesData = new Core.VehicleSales();
        private readonly IVehicleRepository vehicleRepository;

        public HomeController(IVehicleRepository vehicleRepository)
        {
            this.vehicleRepository = vehicleRepository;
        }
 
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult UploadDataFile()
        {
            return View();
        }

        [HttpPost]
        public IActionResult UploadDataFile(List<IFormFile> files)
        {
            vehicleRepository.UploadFiles(files);
            return RedirectToAction("Sales", "Home");
        }


        public IActionResult Sales()
        {
            return View(vehicleRepository.GetAllSales());
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
