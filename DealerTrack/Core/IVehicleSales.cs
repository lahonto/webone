﻿using DealerTrack.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealerTrack.Core
{
    public interface IVehicleSales
    {
        List<VehicleSalesModel> ReadSalesData();
        MostSoldModel MostSoldModel();
        void UploadFile(List<IFormFile> files);
    }
}
