﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DealerTrack.Core
{
    public static class Utils
    {
        public static DateTime EmptyDate = Convert.ToDateTime("1900-01-01");

        public static T SafeConvert<T>(object input)
        {
            if (input == null || string.IsNullOrEmpty(input.ToString()))
            {
                if (typeof(T) == typeof(DateTime))
                {
                    return (T)Convert.ChangeType(EmptyDate.ToString(), typeof(T));
                }
                else if (typeof(T) == typeof(String))
                {
                    return (T)Convert.ChangeType(string.Empty, typeof(T));
                }
                else
                {
                    return default(T);
                }
            }

            T result;
            try
            {
                result = (T)Convert.ChangeType(input.ToString(), typeof(T));
            }
            catch
            {
                result = default(T);
            }
            return result;
        }

        public static string[] Split(string expression, string delimiter, string qualifier, bool ignoreCase)
        {
            string _Statement = String.Format
                ("{0}(?=(?:[^{1}]*{1}[^{1}]*{1})*(?![^{1}]*{1}))",
                                Regex.Escape(delimiter), Regex.Escape(qualifier));

            RegexOptions _Options = RegexOptions.Compiled | RegexOptions.Multiline;
            if (ignoreCase) _Options = _Options | RegexOptions.IgnoreCase;

            Regex _Expression = new Regex(_Statement, _Options);
            return _Expression.Split(expression);
        }

    }
}
