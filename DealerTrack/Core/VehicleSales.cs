﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DealerTrack.Core;
using DealerTrack.Models;
using Microsoft.AspNetCore.Http;

namespace DealerTrack.Core
{
    public class VehicleSales : IVehicleSales
    {
        public VehicleSales()
        {

        }
        public List<VehicleSalesModel> ReadSalesData()
        {
            List<VehicleSalesModel> salesDetails = new List<VehicleSalesModel>();

            try
            {
                using (StreamReader sr = new StreamReader(@"Data\DTData.csv", Encoding.UTF8))
                {
                    string line;
                    var header = 0;

                    while ((line = sr.ReadLine()) != null)
                    {
                        if (header > 0)
                        {
                            var fields = Utils.Split(line, ",", "\"", false);
                            salesDetails.Add(new VehicleSalesModel
                            {
                                DealNumber = fields[0].ToString(),
                                CustomerName = fields[1].ToString(),
                                DealershipName = fields[2].ToString().Replace("\"", ""),
                                Vehicle = fields[3],
                                Price =  Utils.SafeConvert<decimal> (fields[4].ToString().Replace("\"", "")) ,
                                Date = fields[5].ToString()
                            });
                        }
                        header++;
                    }
                }
            }
            catch (FileNotFoundException ex)
            {
                //TODO: logging error
            }
            return salesDetails;
        }
        public MostSoldModel MostSoldModel()
        {
            var list = ReadSalesData();
            MostSoldModel mostSoldModel = new MostSoldModel();

            var result = (from s in list
                          group s by s.Vehicle into g  
                          orderby g.Count() descending 
                          select new { g.Key, Total = g.Count() }) 
                          .Take(1) 
             .ToDictionary(x => x.Key, x => x.Total);
            foreach (KeyValuePair<string, int> item in result)
            {
                mostSoldModel.Vehicle = item.Key;
                mostSoldModel.NumberOfVehiclesSold = item.Value;               
            }

            return mostSoldModel;
        }
        public void UploadFile(List<IFormFile> files)
        {
            long size = files.Sum(f => f.Length);

            var filePaths = new List<string>();
            foreach (var formFile in files)
            {
                if (formFile.Length > 0)
                {
                    // full path to file in temp location
                    var filePath = Path.GetTempFileName();
                    filePath = @"Data\DTData.csv";
                    filePaths.Add(filePath);

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        formFile.CopyToAsync(stream);
                    }
                }
            }
        }

    }
}
